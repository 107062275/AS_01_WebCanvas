# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Rainbow brush                                    | 1~5%     | Y         |


---

### How to use 
![](https://i.imgur.com/Wp4KdYb.jpg)

    右方工具列：
    -  使用筆刷時可以藉由左邊的調色板調整顏色、以及滑桿調整筆刷粗細。
    -  使用彩虹筆時則不需調整顏色。
    -  使用橡皮擦時在畫布上直接做拖拉即可。
    -  使用三種形狀的繪圖工具，在畫布上直接做拖拉即可畫出圖形。
    -  按下重新整理按鈕將清空畫布
    -  輸入文字時，按一下想輸入文字的位置，
    -  在出現的輸入框輸入文字後再按下Enter，即可完成文字的輸入。
    -  輸入文字按鈕下方的兩個下拉式選單則可以選擇字型及大小。

    左方工具列：
    - Undo 及 Redo 按鈕按下時分別可以取消及重做。
    - 按下存檔鈕可以進行存檔。
    - 按下選擇檔案按鈕可以上傳檔案至畫布。


### Function description

    彩虹筆功能：
    主要是利用HSL去實作。
    HSL可以控制色相、飽和度及亮度，我讓一個變數hue在一開始為0，在mouse move 的時候hue會不停的+1（最大加到360），如此便可以讓色相在0-360之間做循環，進而控制筆刷的顏色。

### Gitlab page link

    your web page URL, which should be "https://107062275.gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    由於我的 download button 是採用將連結藏在按鈕中間的方式製作，所以如果按得太邊邊可能會觸發不了，要按中間一點。
    助教們辛苦了，感謝～

<style>
table th{
    width: 100%;
}
</style>
