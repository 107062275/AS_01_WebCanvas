var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");

let isDrawing = false;
let x1 = 0, y1 = 0, x2 = 0, y2 = 0;

ctx.lineJoin = 'round';
ctx.lineCap = 'round';
ctx.lineWidth = 20;
var toolSelect = "none";
let hue = 0

let step = -1;
let userhistory = [];
const undoBtn = document.getElementById("undoBtn");
const redoBtn = document.getElementById("redoBtn");
function push() {
  step++;

  if (step < userhistory.length - 1) {
  userhistory.length = step + 1
  }
  userhistory.push(canvas.toDataURL());
  if (step < userhistory.length && step == 0) {
    undoBtn.disabled = true;
  }else{
    undoBtn.disabled = false;
  }
  if (step == userhistory.length) {
    redoBtn.disabled = true;
  }else{
    redoBtn.disabled = false;
  }
  console.log("push!");
}

push();

//colorselector
var colorBlock = document.getElementById('color-block');
var ctx1 = colorBlock.getContext('2d');
var width1 = colorBlock.width;
var height1 = colorBlock.height;

var colorStrip = document.getElementById('color-strip');
var ctx2 = colorStrip.getContext('2d');
var width2 = colorStrip.width;
var height2 = colorStrip.height;

var colorLabel = document.getElementById('color-label');

var x = 0;
var y = 0;
var drag = false;
var rgbaColor = 'rgba(255,0,0,1)';

ctx1.rect(0, 0, width1, height1);
fillGradient();

ctx2.rect(0, 0, width2, height2);
var grd1 = ctx2.createLinearGradient(0, 0, 0, height1);
grd1.addColorStop(0, 'rgba(255, 0, 0, 1)');
grd1.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
grd1.addColorStop(0.34, 'rgba(0, 255, 0, 1)');
grd1.addColorStop(0.51, 'rgba(0, 255, 255, 1)');
grd1.addColorStop(0.68, 'rgba(0, 0, 255, 1)');
grd1.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
grd1.addColorStop(1, 'rgba(255, 0, 0, 1)');
ctx2.fillStyle = grd1;
ctx2.fill();

function click(e) {
  x = e.offsetX;
  y = e.offsetY;
  var imageData = ctx2.getImageData(x, y, 1, 1).data;
  rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
  fillGradient();
}

function fillGradient() {
  ctx1.fillStyle = rgbaColor;
  ctx1.fillRect(0, 0, width1, height1);

  var grdWhite = ctx2.createLinearGradient(0, 0, width1, 0);
  grdWhite.addColorStop(0, 'rgba(255,255,255,1)');
  grdWhite.addColorStop(1, 'rgba(255,255,255,0)');
  ctx1.fillStyle = grdWhite;
  ctx1.fillRect(0, 0, width1, height1);

  var grdBlack = ctx2.createLinearGradient(0, 0, 0, height1);
  grdBlack.addColorStop(0, 'rgba(0,0,0,0)');
  grdBlack.addColorStop(1, 'rgba(0,0,0,1)');
  ctx1.fillStyle = grdBlack;
  ctx1.fillRect(0, 0, width1, height1);
}

function mousedown(e) {
  drag = true;
  changeColor(e);
}

function mousemove(e) {
  if (drag) {
    changeColor(e);
  }
}

function mouseup(e) {
  drag = false;
}

function changeColor(e) {
  x = e.offsetX;
  y = e.offsetY;
  var imageData = ctx1.getImageData(x, y, 1, 1).data;
  rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
  ctx.strokeStyle = rgbaColor;
  colorLabel.style.backgroundColor = rgbaColor;
}

colorStrip.addEventListener("click", click, false);
colorBlock.addEventListener("mousedown", mousedown, false);
colorBlock.addEventListener("mouseup", mouseup, false);
colorBlock.addEventListener("mousemove", mousemove, false);

//brush size slide bar
var slider = document.getElementById("brushSize");
slider.value = ctx.lineWidth;
slider.oninput = function() {
  ctx.lineWidth = this.value/2;
}

var beforeStep;
canvas.addEventListener('mousedown', (e) => {
  push();
  beforeStep = step;
  if(toolSelect == "brush"){
    isDrawing = true;
    [x1, y1] = [e.offsetX, e.offsetY];
    ctx.globalCompositeOperation = "source-over";  
    ctx.strokeStyle = rgbaColor;
  }else if(toolSelect == "eraser"){
    isDrawing = true;
    [x1, y1] = [e.offsetX, e.offsetY];
    ctx.globalCompositeOperation = "destination-out";  
    ctx.strokeStyle = "rgba(255,255,255,1)";
  }else if(toolSelect == "circle"){
    isDrawing = true;
    [x1, y1] = [e.offsetX, e.offsetY];
    ctx.globalCompositeOperation = "source-over";  
    ctx.strokeStyle = rgbaColor;
  }else if(toolSelect == "rectangle"){
    isDrawing = true;
    [x1, y1] = [e.offsetX, e.offsetY];
    ctx.globalCompositeOperation = "source-over";  
    ctx.strokeStyle = rgbaColor;
  }else if(toolSelect == "triangle"){
    isDrawing = true;
    [x1, y1] = [e.offsetX, e.offsetY];
    ctx.globalCompositeOperation = "source-over";  
    ctx.strokeStyle = rgbaColor;
  }else if(toolSelect == "rainbow"){
    isDrawing = true;
    [x1, y1] = [e.offsetX, e.offsetY];
    ctx.globalCompositeOperation = "source-over";  
  }else{
    console.log("Please select a tool");
  }
  });

canvas.addEventListener('mousemove', (e) =>{
  if (!isDrawing) return;
  ctx.beginPath();
  if(toolSelect == "brush"){
    ctx.moveTo(x1, y1);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.stroke();
    [x1, y1] = [e.offsetX, e.offsetY];
  }else if(toolSelect == "eraser"){
    ctx.moveTo(x1, y1);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.stroke();
    [x1, y1] = [e.offsetX, e.offsetY];
  }else if(toolSelect == "circle"){
    let canvaspic = new Image(); 
    canvaspic.src = userhistory[beforeStep];
    canvaspic.onload = function() {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.drawImage(canvaspic, 0, 0) 
      ctx.globalCompositeOperation = "source-over";
      ctx.arc(x1, y1, Math.sqrt(Math.pow(e.offsetX-x1, 2)+Math.pow(e.offsetY-y1, 2)), 0, 2*Math.PI);
      console.log(beforeStep);
      ctx.stroke();
      }
  }else if(toolSelect == "rectangle"){
    let canvaspic = new Image(); 
    canvaspic.src = userhistory[beforeStep];
    canvaspic.onload = function() {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.drawImage(canvaspic, 0, 0) 
      ctx.globalCompositeOperation = "source-over";
      ctx.rect(x1, y1, e.offsetX-x1, e.offsetY-y1);
      ctx.stroke();
    }
  }else if(toolSelect == "triangle"){
  let canvaspic = new Image(); 
  canvaspic.src = userhistory[beforeStep];
  canvaspic.onload = function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(canvaspic, 0, 0) 
    ctx.globalCompositeOperation = "source-over";
    ctx.moveTo(x1, y1);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.lineTo(2*x1-e.offsetX, e.offsetY);
    ctx.closePath();
    ctx.stroke();
    }
  }else if(toolSelect == "rainbow"){
    ctx.strokeStyle = `hsl(${hue},100%,50%)`;
    hue <= 360 ? hue++ : hue = 0;
    ctx.moveTo(x1, y1);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.stroke();
    [x1, y1] = [e.offsetX, e.offsetY];

  }else {
    console.log("?")
  }
  //ctx.stroke();
});

canvas.addEventListener('mouseup', (e) => {
  isDrawing = false;
});

canvas.addEventListener('mouseout', (e) => {
  isDrawing = false;
});

function toolBrush(){
  toolSelect = "brush";
  canvas.style.cursor = "url(brush.png), auto";
}

function toolEraser(){
  toolSelect = "eraser";
  canvas.style.cursor = "url(eraser.png), auto";
}

function toolCircle(){
  toolSelect = "circle";
  canvas.style.cursor = "url(circle.png), auto";
}

function toolRectangle(){
  toolSelect = "rectangle";
  canvas.style.cursor = "url(rectangle.png), auto";
}

function toolTriangle(){
  toolSelect = "triangle";
  canvas.style.cursor = "url(triangle.png), auto";
}

function toolRainbow(){
  toolSelect = "rainbow";
  canvas.style.cursor = "url(rainbow.png), auto";
}


//reload
function refresh() {
  location.reload();
}

//download
async function downloadImg(el) {
  const imageURI = canvas.toDataURL("image/jpg");
  el.href = imageURI;
};

//upload
let imgInput = document.getElementById('uploadBtn');
  imgInput.addEventListener('change', function(e) {
    if(e.target.files) {
      let imageFile = e.target.files[0];
      var reader = new FileReader();
      reader.readAsDataURL(imageFile);
      reader.onloadend = function (e) {
        var myImage = new Image(); 
        myImage.src = e.target.result; 
        myImage.onload = function(ev) {
          canvas.width = myImage.width;
          canvas.height = myImage.height; 
          ctx.drawImage(myImage,0,0);
          let imgData = canvas.toDataURL("image/jpeg",0.75); 
        }
      }
    }
  });

//input text
let fontStyle;
let fontSize;
let font;
function changeFontStyle(sel){
  fontStyle = sel.options[sel.selectedIndex].text;
  console.log(fontStyle);
}

function changeFontSize(sel){
  fontSize = sel.options[sel.selectedIndex].text;
  console.log(fontSize);
}

var hasInput = false;

function triggerTextInput(){
  hasInput = false;
  console.log(hasInput);
  canvas.style.cursor = "text";
  canvas.onclick = function(e) {
    if (hasInput) return;
    addInput(e.clientX, e.clientY);
  }
  console.log("trigger");
  
}

function addInput(x, y) {
  var input = document.createElement('input');
  input.type = 'text';
  input.style.position = 'fixed';
  input.style.left = (x-4) + 'px';
  input.style.top = (y-4) + 'px';
  input.onkeydown = handleEnter;
  document.body.appendChild(input);
  input.focus();
  hasInput = true;
  console.log("addinput");
}

function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        //hasInput = false;
        push();
    }
    console.log("handle");
}

function drawText(txt, x, y) {
    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    font = fontSize + " " + fontStyle;
    ctx.font = font;
    ctx.fillStyle = rgbaColor;
    console.log(font);
    console.log(ctx.font);
    ctx.fillText(txt, x-400, y-30);

}

//undo&redo
function undo() {

  if (step >= 0) {
  step--;
  ctx.globalCompositeOperation = "source-over"; 
  let canvaspic = new Image(); 
  canvaspic.src = userhistory[step];
  canvaspic.onload = function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(canvaspic, 0, 0) 
    }
  }
  if (step < userhistory.length && step == 0) {
    undoBtn.disabled = true;
  }else{
    undoBtn.disabled = false;
  }
  if (step == userhistory.length) {
    redoBtn.disabled = true;
  }else{
    redoBtn.disabled = false;
  }
}
function redo() {
  if (step >= 0) {
  step++;
  
  let canvaspic = new Image(); 
  canvaspic.src = userhistory[step];
  canvaspic.onload = function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(canvaspic, 0, 0)
    }
  }
  if (step < userhistory.length && step == 0) {
    undoBtn.disabled = true;
  }else{
    undoBtn.disabled = false;
  }
  if (step == userhistory.length) {
    redoBtn.disabled = true;
  }else{
    redoBtn.disabled = false;
  }
}

